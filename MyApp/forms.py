from django.forms import ModelForm
from . import models
from django import forms

class TheForm(ModelForm):
    class Meta:
        model = models.AddStatus
        fields = '__all__'
        
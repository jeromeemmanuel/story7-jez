from django.test import TestCase,Client
from .models import AddStatus
from .views import home
from .forms import TheForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time
import datetime

# Create your tests here.

class Story7UnitTest(TestCase):

    def test_story_7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_7_using_hello_world_template(self):
         response = Client().get('/')
         self.assertTemplateUsed(response, 'MyApp/index.html')

    def test_story_7_landing_page_contains_hello_world(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello",html_response)

    def test_model_can_create_new_status(self):
        #creating a new activity
        new_activity = AddStatus.objects.create(name='asal',dates=datetime.datetime.now())

        #retrieving all available activity
        counting_all_available_status = AddStatus.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_story_7_post_success_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/', {'name' : test, 'dates':datetime.datetime.now()})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response =response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story_7_post_error_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/', {'name': test, 'dates':datetime.datetime.now() })
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story_7_confirmation_page_url_exist(self):
	    response = Client().get('/confirmation_page/')
	    self.assertEqual(response.status_code, 200)

# class Story7FunctionalTest(TestCase):

#     def setUp(self):
#         self.browser = webdriver.Chrome()

#     def test_open_web_and_submit(self):
#         self.browser.get('http://story7-jerome.herokuapp.com/')
        
#         name = self.browser.find_element_by_name('name')
#         status = self.browser.find_element_by_name('message')
#         time.sleep(5)
        
#         name.send_keys('User')
#         status.send_keys('Status')
        
#         name.submit()
#         time.sleep(5)

#         submit1 = self.browser.find_element_by_id('confirm')
#         submit1.submit()


# if __name__ == '__main__': #
#     unittest.main(warnings='ignore') #

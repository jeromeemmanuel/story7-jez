from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import TheForm
from .models import AddStatus

# Create your views here.

# def home(request):
#     if request.method == 'POST':
#         form = TheForm(request.POST)
#         if form.is_valid():
#             data_item = form.save(commit=False)
#             data_item.save()
#             return render(request, 'MyApp/index.html')
#     allStatus = AddStatus.objects.all()
#     form = TheForm()
#     return render(request, 'MyApp/index.html', {'form' : form, 'allStatus':allStatus})

data_list = [{}]

def home(request):
	if request.method == 'POST':
		form = TheForm(request.POST)
		data_list[0]["name"] = request.POST.get("name")
		data_list[0]["message"] = request.POST.get("message")

		if form.is_valid():
			return redirect("/confirmation_page/")
		else:
			return render(request, 'MyApp/index.html', {"data":AddStatus.objects.all().order_by('-dates')})

	else:
			return render(request, 'MyApp/index.html', {"data":AddStatus.objects.all().order_by('-dates')})


def confirmation(request):
	if request.method == 'POST':
		form = TheForm(request.POST)
		form.save()
		return redirect("/")
	else:
		return render(request, 'MyApp/confirm.html', {"data_list" : data_list})

def cancel(request):
	form_data = [{}]
	return home(request)

